
#include <stdio.h>
#include <semaphore.h> //Biblioteca utilizada para controle de acesso a Zona Critica(Variaveis com valor de saldo das contas)
#include <pthread.h> //Biblioteca utilizada para substituir outras bibliotecas e permitir implementacao de threads

struct c {
  int saldo;
  };

typedef struct c conta;

conta from, to;

int valor;
int i;
sem_t semaphore;

// The child thread will execute this function
void* transferencia( void *arg){
  sem_wait(&semaphore); //wait state
  if (from.saldo >= valor){ // 2
      from.saldo -= valor;
      to.saldo += valor;
    }
    printf("Transferência concluída com sucesso!\n");
    printf("Saldo de c1: %d\n", from.saldo);
    printf("Saldo de c2: %d\n", to.saldo);
    sem_post(&semaphore);
}
  int main(){
    sem_init(&semaphore, 0, 1);
    pthread_t th[10];
    from.saldo = 100;
    to.saldo = 100;
    printf( "Transferindo 10 para a conta c2\n" );
    valor = 10;
    for(i=0; i < 10; i++){
    pthread_create(&(th[i]),NULL,transferencia,NULL);
   }

    for (i=0;i<10;i++){
    pthread_join(th[i],NULL);
    }
    sem_destroy(&semaphore);

    printf("Transferências concluídas.\n");
    return 0;
}
